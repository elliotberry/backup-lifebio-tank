# Life Biosciences

## Vagrant-specific Setup Instructions

1. If using vagrant for development, run `vagrant up` from the root directory to provision a
development server. [Scotchbox](https://box.scotch.io/) is used by default so refer to the website
for server default settings (mysql passwords, etc.)

⚠️ We changed the host from the Scotchbox default to `192.168.33.12`.

3. SSH into the development server using `vagrant ssh` to setup the wordpress database.
`mysql -u root -p` using the password "root" and create a new database
`CREATE DATABASE lifebiosciences;`

4. Import the mysql database dump from a friend (or staging I guess) into your new database.

5. Update your `etc/hosts` so the host points to [lifebio.local](//lifebio.local). Update the
Site URL value in the db.

## Getting up and running

1. Go to the theme directory: `cd webroot/wp-content/themes/life-bio-sciences/`
2. Read the [readme](webroot/wp-content/themes/life-bio-sciences/README.md) there.
