# Life Biosciences Custom WP Theme

Built with Sage. Upgraded from Sage 8 to Sage 9. Many things have been omitted from a default Sage
install, however.

## Prerequisites:

- node, (v6.9.4 at least)
  - Might run painfully slow on Node ~8.10. And ^9.0.0. [See this GitHub thread for details.](https://github.com/nodejs/node/issues/19769). But it's fine in 10.

## 🏃 Up and Running

1. Run `npm install` to install front-end dependencies.
2. Some helpful commands:

- For Developing: `npm run start`
  - ~~BrowserSync / Hot Reloading: you will need to be on `localhost:3000`~~
    - Static assets seem to load slower on the proxied `localhost:3000`, but hot reloading won't
      work on the older `lifebio.local` (or your personal equivalent)
- For Compiling: `npm run build`
  - Actually exports some stylesheets.
- Compiling for Production: `npm run build:production`
  - Minifies JS/CSS
