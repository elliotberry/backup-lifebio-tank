<?php
/**
 * Template Name: Companies Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page-companies-new'); ?>
<?php endwhile; ?>
