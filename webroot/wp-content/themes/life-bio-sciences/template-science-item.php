<?php
/**
 * Template Name: Science Subpage Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page-science-item'); ?>
<?php endwhile; ?>
