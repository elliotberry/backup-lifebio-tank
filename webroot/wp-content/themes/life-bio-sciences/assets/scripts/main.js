// import external dependencies
import "jquery";

import "./read-more.js";

/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {
  "use strict";

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    common: {
      init: function() {
        // JavaScript to be fired on all pages

        /* Main menu toggle */
        /* https://codepen.io/svinkle/pen/LDKwA */
        var header = document.querySelector(".banner .container");
        var menu = document.querySelector(".nav-primary");
        var main = document.querySelector(".wrap");
        var footer = document.querySelector(".content-info");
        var menuToggle = document.createElement("a");

        function hideMobileMenu() {
          menu.classList.remove("active");
          menu.setAttribute("aria-hidden", "true");
          main.setAttribute("aria-hidden", "false");
          footer.setAttribute("aria-hidden", "false");
          menuToggle.setAttribute("aria-expanded", "false");
        }

        function showMobileMenu() {
          menu.classList.add("active");
          menu.setAttribute("aria-hidden", "false");
          main.setAttribute("aria-hidden", "true");
          footer.setAttribute("aria-hidden", "true");
          menuToggle.setAttribute("aria-expanded", "true");
        }

        function isHidden(element) {
          var style = window.getComputedStyle(element);
          return style.display === "none";
        }

        function isMobile() {
          // since our MQs are in ems, instead of checking for the screen width we will instead look to see if the mobile menu toggle is visible
          return !isHidden(menuToggle);
        }

        function checkMobileMenu() {
          if (!isMobile()) {
            menu.setAttribute("aria-hidden", "false");

            if (isHidden(main)) {
              // if main is hidden (as when the mobile menu is toggled) then disable the mobile menu
              hideMobileMenu();
            }
          }
        }

        function initMenu() {
          // button properties
          menuToggle.classList.add("nav-toggle");
          menuToggle.setAttribute("href", "#nav-primary");
          menuToggle.setAttribute("id", "nav-toggle");
          menuToggle.setAttribute("aria-label", "Menu");
          menuToggle.setAttribute("aria-expanded", "false");
          menuToggle.setAttribute("aria-controls", "nav-primary");
          menuToggle.innerHTML =
            '<span></span><span></span><span></span><span></span><div class="screen-reader-text">Show the menu</div>';

          // menu properties
          menu.setAttribute("aria-hidden", "true");
          menu.setAttribute("aria-labeledby", "nav-toggle");

          // add toggle button to page
          header.insertBefore(menuToggle, menu);

          // handle button click event
          menuToggle.addEventListener(
            "click",
            function(e) {
              e.preventDefault();

              if (menu.classList.contains("active")) {
                hideMobileMenu();
              } else {
                showMobileMenu();
              }
            },
            false
          );

          // check for desktop sizing to disble mobile menu
          setInterval(checkMobileMenu, 180);
        }

        // setup the mobile menu and menu toggle button
        initMenu();

        // check to see if submenu is visible (submenus are always visible, not interaction dependent)
        // how to move this calc into the header template...?
        if (
          $("#menu-main-menu .menu-item-has-children.current-menu-item ")
            .length > 0 ||
          $("#menu-main-menu .menu-item-has-children.current-menu-ancestor ")
            .length > 0 ||
          $("#menu-main-menu .menu-item-has-children.current-page-ancestor ")
            .length > 0
        ) {
          $(".banner").addClass("submenu-is-visible"); // increases the height of the desktop banner
        }

        // Focus Area and Company wheel
        $(".cool-new-wheel-blob").on("click", function(e) {
          if (!$("body").hasClass("big-blob-is-active")) {
            var $thisElem = $(e.currentTarget);
            var $data = $thisElem.attr("data");
            $(".cool-new-wheel-blob").removeClass("is--active");
            $("body").addClass("big-blob-is-active");
            $thisElem.addClass("is--active");
            $('.wheel-popup[data="' + $data + '"]').addClass("is--active");
          } else {
            // If the wheel is open AND it's a company wheel with a defined URL provided,
            // link link away.

            var $thisElem = $(e.currentTarget);

            var $companyLogo = $thisElem.children("a.area-logo"); // only companies will have this.
            var $companyUrl = $companyLogo.attr("data-href");
            // If URL is provided, link up the Logo when the Focus Area Popup is Active
            if ($companyLogo && !!$companyUrl) {
              $companyLogo.attr("href", $companyUrl);
            }
          }
        });

        // 'X' out of Focus Area / Companies popups.
        $(".back").on("click", function() {
          xPopup();
        });

        // Also exit the popup on Exit key.
        $(document).keyup(e => {
          if (e.keyCode == 27 && $("body").hasClass("big-blob-is-active")) {
            xPopup();
          }
        });

        function xPopup() {
          $(".cool-new-wheel-blob").removeClass("is--active");
          $(".wheel-popup").removeClass("is--active");
          $("body").removeClass("big-blob-is-active");
        }
        /**
         * Accessible Tabs
         * This content is licensed according to the W3C Software License at
         * https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
         */
        function initTabs() {
          var tablist = document.querySelectorAll('[role="tablist"]')[0];
          var tabs;
          var panels;

          // For easy reference
          var keys = {
            end: 35,
            home: 36,
            left: 37,
            up: 38,
            right: 39,
            down: 40,
            enter: 13,
            space: 32
          };

          // Add or substract depending on key pressed
          var direction = {
            37: -1,
            38: -1,
            39: 1,
            40: 1
          };

          function generateArrays() {
            tabs = document.querySelectorAll('[role="tab"]');
            panels = document.querySelectorAll('[role="tabpanel"]');
          }

          // Deactivate all tabs and tab panels
          function deactivateTabs() {
            for (var t = 0; t < tabs.length; t++) {
              tabs[t].setAttribute("tabindex", "-1");
              tabs[t].setAttribute("aria-selected", "false");
            }

            for (var p = 0; p < panels.length; p++) {
              panels[p].setAttribute("hidden", "hidden");
            }
          }

          // Activates any given tab panel
          function activateTab(tab, setFocus) {
            setFocus = setFocus || true;
            // Deactivate all other tabs
            deactivateTabs();

            // Remove tabindex attribute
            tab.removeAttribute("tabindex");

            // Set the tab as selected
            tab.setAttribute("aria-selected", "true");

            // Get the value of aria-controls (which is an ID)
            var controls = tab.getAttribute("aria-controls");

            // Remove hidden attribute from tab panel to make it visible
            document.getElementById(controls).removeAttribute("hidden");

            // Set focus when required
            if (setFocus) {
              tab.focus();
            }
          }

          // When a tab is clicked, activateTab is fired to activate it
          function clickEventListener(event) {
            var tab = event.target;
            activateTab(tab, false);
          }

          // Make a guess
          function focusFirstTab() {
            tabs[0].focus();
          }

          // Make a guess
          function focusLastTab() {
            tabs[tabs.length - 1].focus();
          }

          // Either focus the next, previous, first, or last tab
          // depening on key pressed
          function switchTabOnArrowPress(event) {
            var pressed = event.keyCode;

            if (direction[pressed]) {
              var target = event.target;
              if (target.index !== undefined) {
                if (tabs[target.index + direction[pressed]]) {
                  tabs[target.index + direction[pressed]].focus();
                } else if (pressed === keys.left || pressed === keys.up) {
                  focusLastTab();
                } else if (pressed === keys.right || pressed === keys.down) {
                  focusFirstTab();
                }
              }
            }
          }
          // When a tablist's aria-orientation is set to vertical,
          // only up and down arrow should function.
          // In all other cases only left and right arrow function.
          function determineOrientation(event) {
            var key = event.keyCode;
            var vertical =
              tablist.getAttribute("aria-orientation") === "vertical";
            var proceed = false;

            if (vertical) {
              if (key === keys.up || key === keys.down) {
                event.preventDefault();
                proceed = true;
              }
            } else {
              if (key === keys.left || key === keys.right) {
                proceed = true;
              }
            }

            if (proceed) {
              switchTabOnArrowPress(event);
            }
          }

          // Handle keydown on tabs
          function keydownEventListener(event) {
            var key = event.keyCode;

            switch (key) {
              case keys.end:
                event.preventDefault();
                // Activate last tab
                focusLastTab();
                break;
              case keys.home:
                event.preventDefault();
                // Activate first tab
                focusFirstTab();
                break;

              // Up and down are in keydown
              // because we need to prevent page scroll >:)
              case keys.up:
              case keys.down:
                determineOrientation(event);
                break;
            }
          }

          // Handle keyup on tabs
          function keyupEventListener(event) {
            var key = event.keyCode;

            switch (key) {
              case keys.left:
              case keys.right:
                determineOrientation(event);
                break;
              case keys.enter:
              case keys.space:
                activateTab(event.target);
                break;
            }
          }

          function addListeners(index) {
            tabs[index].addEventListener("click", clickEventListener);
            tabs[index].addEventListener("keydown", keydownEventListener);
            tabs[index].addEventListener("keyup", keyupEventListener);

            // Build an array with all tabs (<button>s) in it
            tabs[index].index = index;
          }

          generateArrays();

          // Bind listeners
          for (var i = 0; i < tabs.length; ++i) {
            addListeners(i);
          }
        }

        initTabs();
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = funcname === undefined ? "init" : funcname;
      fire = func !== "";
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === "function";

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire("common");

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, "_").split(/\s+/), function(
        i,
        classnm
      ) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, "finalize");
      });

      // Fire common finalize JS
      UTIL.fire("common", "finalize");
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);
})(jQuery); // Fully reference jQuery after this point.
