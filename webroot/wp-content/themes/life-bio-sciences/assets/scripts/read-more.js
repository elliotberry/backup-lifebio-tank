(function($) {
  /**
   * Team Display: Read More toggle
   */
  $(".js-read-more-toggle").click(e => {
    $(e.currentTarget)
      .prev()
      .toggleClass("is-expanded");
  });
})(jQuery);
