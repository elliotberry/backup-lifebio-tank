<?php
/**
 * Template Name: Leadership Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page-leadership'); ?>
<?php endwhile; ?>
