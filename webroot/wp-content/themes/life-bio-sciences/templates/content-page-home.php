<?php get_template_part( 'templates/modules/hero' ); ?>
<?php get_template_part( 'templates/modules/cta' ); ?>
<?php get_template_part( 'templates/modules/focus-area', 'icons-list' ); ?>
<?php
  $section_var = 'image_text_2';
  include( locate_template( 'templates/modules/image-text.php' ) );
?>
<?php get_template_part( 'templates/modules/posts', 'recent' ); ?>
