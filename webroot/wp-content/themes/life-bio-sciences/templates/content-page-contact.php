<?php get_template_part( 'templates/modules/hero' ); ?>

<section class="default-content">
  <div class="container">
    <?php the_content(); ?>
  </div>
</section>
