<?php
  $offsite_info = '';
  if ( get_field('offsite_link') ) {
    $offsite_info = '<a class="offsite-link" target="_blank" rel="noopener" href="' . get_field('offsite_url') . '">' . get_field('resource_name') . '</a>';
  }
?>

<time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
<p class="post-category"><?php echo get_the_category_list( ", " ); ?> <?php echo $offsite_info; ?></p>
