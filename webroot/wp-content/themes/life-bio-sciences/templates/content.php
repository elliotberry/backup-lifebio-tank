<?php
  /**
   * Shared between "News" Page and the "Posts Recent" module on the Home Page.
   */

  $link = get_the_permalink();
  $link_extra = '';

  if ( get_field('offsite_link') ) {
    $link = get_field('offsite_url');
    $link_extra = ' target="_blank" rel="noopener"';
  }

  $thumb_id = get_post_thumbnail_id();
?>

<article <?php post_class(); ?>>
  <?php if($thumb_id): ?>
    <div class="entry-banner">
      <?php the_post_thumbnail('large'); ?>
    </div>
  <?php endif; ?>
  <header>
    <h3 class="entry-title"><a href="<?php echo $link; ?>" <?php echo $link_extra; ?>><?php the_title(); ?></a></h3>
    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
    <a class="button-cta" href="<?php echo $link; ?>" <?php echo $link_extra; ?>><?php _e('Read Full Article', 'sage'); ?></a>
  </div>
</article>
