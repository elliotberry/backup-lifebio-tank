<?php while (have_posts()):
  the_post(); ?>
  <?php
  $link = get_the_permalink();
  $link_extra = '';

  if (get_field('offsite_link')) {
    $link = get_field('offsite_url');
    $link_extra = ' target="_blank" rel="noopener"';
  }
  ?>
  <nav class="post-top-nav"><a href="<?php echo get_the_permalink(
    get_option('page_for_posts')
  ); ?>" class="button-back">News</a></nav>
  <article <?php post_class(); ?>>
    <?php get_template_part('templates/entry-hero-banner'); ?>

    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
      <?php get_template_part('templates/modules/sharing-buttons'); ?>
    </div>
    <footer>
      <?php wp_link_pages([
        'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'),
        'after' => '</p></nav>'
      ]); ?>
    </footer>
    <?php
  // comments_template('/templates/comments.php');
  ?>
  </article>
<?php
endwhile; ?>
