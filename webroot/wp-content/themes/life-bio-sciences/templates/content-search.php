<?php
  $link = get_the_permalink();
  $link_extra = '';

  if ( get_field('offsite_link') ) {
    $link = get_field('offsite_url');
    $link_extra = ' target="_blank" rel="noopener"';
  }
?>
<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php
      if (get_post_type() === 'post') {
        get_template_part('templates/entry-meta');
      } else {
        get_template_part('templates/entry-meta-other');
      }
    ?>
  </header>
  <div class="entry-summary">
    <?php // the_excerpt(); ?>
  </div>
</article>
