<footer id="content-info" class="content-info">
  <div class="container">
    <div class="row">
      <nav class="col-12 nav-secondary" id="nav-secondary">
        <?php
        if (has_nav_menu('secondary_navigation')) :
          wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav']);
        endif;
        ?>
      </nav>
    </div>
    <div class="row">
      <div class="col-6 copyright">&copy; <?php echo date('Y'); ?> Life Biosciences, LLC. All rights reserved.</div>

      <?php
      $facebook = get_field( 'facebook_url', 'option' );
      $linkedin = get_field( 'linkedin_url', 'option' );
      $twitter = get_field( 'twitter_url', 'option');
       ?>

      <div class="col-6 social-bar">
        <a class="icon icon--linkedin" href="<?php echo $linkedin ?>">
          <span class="sr-only">LinkedIn</span>
        </a>

        <a class="icon icon--facebook" href="<?php echo $facebook ?>">
          <span class="sr-only">Facebook</span>
        </a>

        <a class="icon icon--twitter" href="<?php echo $twitter ?>">
          <span class="sr-only">Twitter</span>
        </a>
      </div>

    </div>
  </div>
</footer>


<!-- Start Open Web Analytics Tracker -->
<script type="text/javascript">
//<![CDATA[
var owa_baseUrl = 'http://lifebiosciences.com/owa/';
var owa_cmds = owa_cmds || [];
owa_cmds.push(['setSiteId', 'ecbcbb056346afb80ac141983f721256']);
owa_cmds.push(['trackPageView']);
owa_cmds.push(['trackClicks']);

(function() {
	var _owa = document.createElement('script'); _owa.type = 'text/javascript'; _owa.async = true;
	owa_baseUrl = ('https:' == document.location.protocol ? window.owa_baseSecUrl || owa_baseUrl.replace(/http:/, 'https:') : owa_baseUrl );
	_owa.src = owa_baseUrl + 'modules/base/js/owa.tracker-combined-min.js';
	var _owa_s = document.getElementsByTagName('script')[0]; _owa_s.parentNode.insertBefore(_owa, _owa_s);
}());
//]]>
</script>
<!-- End Open Web Analytics Code -->
