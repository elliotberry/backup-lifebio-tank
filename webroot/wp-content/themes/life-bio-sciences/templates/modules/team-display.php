<section class="team-grid">
  <div class="container">
    <?php
      if ( have_rows('management_team_groups') ) :
        // tabs
        $group_count = 0;
        echo '<div class="tablist" role="tablist" aria-label="' . __( 'Leadership Groups', 'sage' ) . '">';

        while( have_rows('management_team_groups') ) : the_row();
          $group_name = get_sub_field('group_name');

          echo '<button
            role="tab"
            aria-selected="' . ( ( $group_count == 0 ) ? 'true': 'false' ) . '"
            aria-controls="tabpanel-' . $group_count . '"
            id="tab-' . $group_count . '"
            >' . $group_name . '</button>';
          $group_count ++;
        endwhile;
        echo '</div>';

        // tab panels
        $group_count = 0;

        while( have_rows('management_team_groups') ) : the_row();
          $group_name = get_sub_field('group_name');

          echo '<div
            class="team-group"
            role="tabpanel"
            id="tabpanel-' . $group_count . '"
            aria-labelledby="tab-' . $group_count . '"
            ' . ( ( $group_count == 0 ) ? '' : 'hidden=""' ) . '
            >';
          echo '  <div class="team-members">';

          if ( have_rows('members') ) :
            while( have_rows('members') ) : the_row();
              $name  = get_sub_field('name');
              $title = get_sub_field('title');
              $text  = get_sub_field('text');
              $social_linkedin  = get_sub_field('social_linkedin');
              $image  = get_sub_field('image'); ?>

              <div class="member">
                <div class="image"><?php echo wp_get_attachment_image( $image, 'medium' ); ?></div>
                <div class="content">
                  <div class="member-name-wrap">
                    <h3 class="member-name"><?php echo $name; ?></h3>
                    <div class="social-icon-wrap">
                      <?php if ( $social_linkedin ): ?>
                        <a class="social-icon linkedin" href="<?php echo $social_linkedin ?>" target="_blank" rel="noopener"><span class="sr-only">LinkedIn</span></a>
                      <?php endif ?>
                    </div>
                  </div>
                  <p class="member-title"><?php echo $title; ?></p>
                  <div class="description">
                    <?php echo $text ?>
                  </div>
                  <button class="js-read-more-toggle button-cta" aria-hidden="true">Read</button>
                </div>
              </div>
              <?php
            endwhile;
          endif;

          echo '  </div>';
          echo '</div>';

          $group_count ++;
        endwhile;
      endif;
    ?>
  </div>
</section>
