
<div class="bubble-container">
<?php
  /**
   * Returns SVGs for bubbles, based on page title.

   * SVG's are layered on top of each other. Both must have `.bubbles`, and the fill circles
   * should have `bubbles bubbles--screen` in the class attribute.
   */

 $page_title = strtolower(get_the_title());
if ($page_title === 'home') { ?>
  <div class="bubbles">
    <svg viewBox="0 0 400 400">
      <circle cx="116.1" cy="245.1" r="58.6" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
      <circle cx="293" cy="108.1" r="38" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
      <circle cx="89.4" cy="92.5" r="42.3" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
      <circle cx="352.1" cy="183.2" r="30.7" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
      <circle cx="251.8" cy="62.4" r="23.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
      <circle cx="110.2" cy="155" r="23.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
      <circle cx="234.1" cy="115.9" r="101.3" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="10.3"/>
      <circle cx="121.8" cy="284.5" r="101.3" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="10.3"/>
    </svg>
  </div>
  <div class="bubbles bubbles--screen">
    <svg viewBox="0 0 400 400">
      <circle cx="119.5" cy="285.1" r="101.3" fill="#0077a3" opacity=".8" />
      <circle cx="234.1" cy="115.9" r="101.3" fill="#0077a3" opacity=".8" />
    </svg>
  </div>
<?php } elseif ($page_title === 'about us') {
  ?>

  <div class='bubbles'>
    <svg viewBox="0 0 405.7 401">
      <g>
        <circle cx="190.4" cy="126.8" r="58.7" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="149.7" cy="276.7" r="42.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="25.4" cy="119.4" r="23.5" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="374" cy="168.2" r="23.5" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="93.8" cy="121.8" r="36.5" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="184.2" cy="336.4" r="23.5" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="152.6" cy="129.1" r="101.5" transform="rotate(-45 152.6 129.1)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="10.3"/>
        <circle cx="299" cy="271.9" r="101.5" transform="rotate(-45 299 272)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="10.3"/>
      </g>
    </svg>
  </div>
  <div class="bubbles bubbles--screen">
    <svg viewBox="0 0 405.7 401">
      <g fill="#0077a3" opacity=".8">
        <circle cx="153.1" cy="129.1" r="101.3"  />
           <circle cx="299" cy="271.9" r="101.3"  />
      </g>
    </svg>
  </div>

  <?php
} elseif ($page_title === 'companies') { ?>
    <div class="bubbles">
      <svg viewBox="0 0 400 400">
        <g>
          <circle cx="278.5" cy="155.9" r="58.6" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
          <circle cx="101.6" cy="292.9" r="38" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
          <circle cx="305.3" cy="308.5" r="42.3" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
          <circle cx="42.5" cy="217.8" r="30.7" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
          <circle cx="142.8" cy="338.5" r="23.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
          <circle cx="284.5" cy="246" r="23.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
          <circle cx="160.5" cy="285.1" r="101.3" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="10.3"/>
          <circle cx="272.8" cy="116.5" r="101.3" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="10.3"/>
        </g>
      </svg>
    </div>
    <div class="bubbles bubbles--screen">
      <svg viewBox="0 0 400 400">
        <circle cx="160.5" cy="285.1" r="101.3" fill="#00adee" opacity=".6" />
        <circle cx="275.2" cy="115.9" r="101.3" fill="#00adee" opacity=".6" />
      </svg>
    </div>
  <?php } elseif ($page_title === 'focus areas') { ?>
    <div class="bubbles">
      <svg viewBox="0 0 400 400">
        <circle cx="123.2" cy="156.4" r="58.6" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="300.2" cy="293.4" r="38" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="96.5" cy="309" r="42.3" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="359.3" cy="218.3" r="30.7" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="258.9" cy="339" r="23.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="117.3" cy="246.5" r="23.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="241.3" cy="285.6" r="101.3" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="10.3"/>
        <circle cx="129" cy="117" r="101.3" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="10.3"/>
      </svg>
    </div>
    <div class="bubbles bubbles--screen">
      <svg viewBox="0 0 400 400">
        <circle cx="126.6" cy="116.4" r="101.3" fill="#329d9a"/>
        <circle cx="241.3" cy="285.6" r="101.3" fill="#329d9a"/>
      </svg>
    </div>
  <?php } elseif ($page_title === 'team') { ?>
    <div class="bubbles">
      <svg viewBox="0 0 400 400">
        <circle cx="266.1" cy="261.6" r="42.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="94.4" cy="365.2" r="23.5" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="108.2" cy="74.6" r="23.5" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="155.3" cy="74.6" r="23.5" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="60.9" cy="113.1" r="36.5" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="131.7" cy="115.1" r="23.5" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="120" cy="240.2" r="101.5" transform="rotate(-45 120 240.2)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="10.3"/>
        <circle cx="280" cy="112.8" r="101.5" transform="rotate(-45 280 112.9)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="10.3"/>
      </svg>
    </div>
    <div class="bubbles bubbles--screen">
      <svg viewBox="0 0 400 400">
        <circle cx="120" cy="240.2" r="101.5" transform="rotate(-45 120 240.2)" fill="#7acd63"/>
        <circle cx="280" cy="112.8" r="101.5" transform="rotate(-45 280 112.9)" fill="#7acd63"/>
      </svg>
    </div>
  <?php } elseif ($page_title === 'contact') { ?>
    <div class="bubbles">
      <svg viewBox="0 0 400 400">
        <circle cx="296.7" cy="186.2" r="96.8" transform="rotate(-45 296.6 186.2)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="10.3"/>
        <circle cx="103.3" cy="211.3" r="96.8" transform="rotate(-45 103.2 211.3)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="10.3"/>
        <circle cx="216.1" cy="319.8" r="56" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="187.9" cy="96.8" r="40.5" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="297.3" cy="311.9" r="22.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="125.5" cy="90.7" r="22.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="136.6" cy="328.8" r="22.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
        <circle cx="155.6" cy="369.3" r="22.4" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="3.8"/>
      </svg>
    </div>
    <div class="bubbles bubbles--screen">
      <svg viewBox="0 0 400 400">
        <g fill="#00aeef">
          <circle cx="296.7" cy="186.2" r="96.8" transform="rotate(-45 296.6 186.2)"/>
          <circle cx="103.3" cy="211.3" r="96.8" transform="rotate(-45 103.2 211.3)"/>
        </g>
      </svg>
    </div>
    <?php

  } elseif (is_home()) { ?>
    <div class="bubbles">
      <svg viewBox="0 0 400 400">
        <g fill="none" stroke="#fff" stroke-miterlimit="10">
          <circle cx="133.9" cy="138.4" r="42.4" stroke-width="3.8"/>
          <circle cx="305.6" cy="34.8" r="23.5" stroke-width="3.8"/>
          <circle cx="291.8" cy="325.4" r="23.5" stroke-width="3.8"/>
          <circle cx="244.7" cy="325.4" r="23.5" stroke-width="3.8"/>
          <circle cx="339.1" cy="286.9" r="36.5" stroke-width="3.8"/>
          <circle cx="268.3" cy="284.9" r="23.5" stroke-width="3.8"/>
          <circle cx="280" cy="159.8" r="101.5" transform="rotate(-45 280 159.8)" stroke-width="10.3"/>
          <circle cx="120" cy="287.2" r="101.5" transform="rotate(-45 120 287.1)" stroke-width="10.3"/>
        </g>
      </svg>
    </div>
    <div class="bubbles bubbles--screen">
      <svg viewBox="0 0 400 400">
        <g fill="#48bd9f">
          <circle cx="280" cy="159.8" r="101.5" transform="rotate(-45 280 159.8)"/>
          <circle cx="120" cy="287.2" r="101.5" transform="rotate(-45 120 287.1)"/>
        </g>
      </svg>
    </div>
<?php }
?>
</div>
