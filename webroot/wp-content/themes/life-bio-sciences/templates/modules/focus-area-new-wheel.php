<div class="cool-new-wheel-spacing">
  <div class="cool-new-wheel-container">
  <?php
  $htmlstr = "";

  // focus areas
  $args = array(
    'posts_per_page'   => -1,
    'post_type'    => 'lifebio_focusarea',
    'post_status'  => 'publish',
    'order'        => 'ASC',
    'orderby'      => 'menu_order',
  );
  $query = new WP_Query( $args );

  $excludethis = get_field('exclude_focus_areas', 'option');

  if ( $query->have_posts() ) :
    $i = 1;
    while ( $query->have_posts() ) : $query->the_post();
    $area_id = get_the_ID();

    if ($area_id !== $excludethis) {
      $area_name        = get_the_title();
      $area_image       = wp_get_attachment_image( get_post_thumbnail_id( $area_id ), 'original' );
      $area_color       = get_field( 'color' );
      $area_logo        = get_field( 'logo' );
      $area_logo_small  = get_field( 'small_logo' );
      $area_iden = "circle" . $i;

      if (get_field( 'summary' )) {
        $desc = get_field( 'summary' );
      }
      else {
        $desc = get_the_content();
      }
  ?>

  <div class="cool-new-wheel-blob <?php echo $area_iden; ?>" style="color:<?php echo $area_color; ?>; border-color:<?php echo $area_color; ?>;" data="<?php echo $area_iden ?>"><span><div class="plus">+</div><div class="area-name"><?php echo $area_name ?></div><div class="focus-area-logo-small" style="background-image:url('<?php echo $area_logo_small; ?>');"></div></span></div>
  <?php
  $htmlstr = $htmlstr . '<div class="wheel-popup ' . $area_iden . '" data="' . $area_iden . '" style="background: ' . $area_color . '"><div class="area-desc"><div class="area-name">' . $area_name . '</div>' . $desc . '</div><a class="back"></a></div>';


  $i++;
          }
          endwhile;
          wp_reset_postdata();
        endif;

        ?>
    <img class="middle-logo" src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/logo-life-biosciences.svg" alt="Life Biosciences logo">
    <div class="popup-container">
      <?php echo $htmlstr; ?>
    </div>
  </div>
</div>
