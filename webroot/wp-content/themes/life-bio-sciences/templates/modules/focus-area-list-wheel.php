<section class="focus-area-list real-one">


  <div class="container">

    <?php

      $text        = get_field('content');

      echo '<div class="wheel-content-wrap">';
      echo '  <div class="text-content">';

      // text
      if ( $title ) {
        echo '<h2 class="section-title">' . $title . '</h2>';
      }
      if ( $text ) {
        echo $text;
      }

      echo '  </div>';

      // wheel
      get_template_part( 'templates/modules/focus-area', 'new-wheel' );
      ?>

  </div>
  </div>
</section>
