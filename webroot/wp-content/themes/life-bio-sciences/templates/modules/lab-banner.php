<section class="labs-banner">
  <div class="lab-wrap">
    <div class="content">
      <p><a href="<?php echo get_permalink(139); ?>" class="button-back"><?php _e( 'Return to Labs', 'sage' ); ?></a></p>
      <?php the_content(); ?>

      <div class="contact-info">
        <?php the_field('contact_information'); ?>
      </div>

      <?php
        $gallery = get_field('image_gallery');
        if ( $gallery ) :
      ?>
        <div class="lab-gallery">
          <?php
            foreach( $gallery as $image ) {
              echo '<div class="slide">';
              echo '<img alt="' . $image['alt'] . '" src="' . $image['sizes']['lbs-lab-thumbnail'] . '">';
              echo '</div>';
            }
          ?>
        </div>
      <?php endif; ?>
    </div>
    <div class="lab-map">
      <div class="background-image">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/lab-map.svg" alt="">
      </div>
   </div>
  </div>
</section>