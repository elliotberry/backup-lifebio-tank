<section class="only-on-mobile company-list company-list-mobile">
  <div class="container">


    <?php
      $company_counter = 0;


      $args = array(
        'post_type'      => 'lifebio_company',
        'post_status'    => 'publish',
        'order'          => 'ASC',
        'orderby'        => 'menu_order',
        'posts_per_page' => 1000,
      );
      $query = new WP_Query( $args );

      if ( $query->have_posts() ) :
        while ( $query->have_posts() ) : $query->the_post();
          $name     = get_the_title();
          $location = get_field( 'location' );
          $url      = get_field( 'url' );
        if ($name) {  $cid = slugify($name); }

          echo '<div class="company" data="' . $cid . '">';
          echo '  <a id="lab-' . $company_counter . '" class="jump-anchor"></a>';

          // name, location, website
          echo '  <h2 class="company-name"><strong>' . $name . '</strong>';
          if ( $location ) {
            echo ' &nbsp;|&nbsp; ' . $location;
          }

          if ( $url ) {
            echo ' &nbsp;|&nbsp; <a class="icon-external" href="' . $url . '" target="_blank" rel="noopener"><span class="sr-only">' . __( 'Website', 'sage' ) . '</span></a>';
          }
          echo '</h2>';

          // description
          the_content();

          // focus areas
          if( have_rows('focus_areas') ):
            echo '<div class="focus-areas">';

            while ( have_rows('focus_areas') ) : the_row();
              $area          = get_sub_field( 'focus_area' );
              $area_id       = $area->ID;
              $area_name     = $area->post_title;
              $area_image    = wp_get_attachment_image( get_post_thumbnail_id( $area_id ), 'original' );
              $area_color    = get_field( 'color', $area_id );
              $area_desc     = get_sub_field( 'text' );

              echo '<div class="focus-area">';
              echo '  <h3 class="area-name"><strong>' . __( 'Aging Component:', 'sage' ) . '</strong> <span style="color:' . $area_color . '">' . $area_name . '</span></h3>';
              echo '  <div class="area-details">';
              echo "    <div class='image' style='border-color: $area_color'>";
              echo $area_image;
              echo '    </div>';
              echo '    <div class="text">';
              echo $area_desc;

              echo '    </div>';
              echo '  </div>';
              echo '</div>';
            endwhile;

            echo '</div>';
          endif;

          echo '</div>';

          $company_counter ++;

        endwhile;
        wp_reset_postdata();
      endif;
    ?>
  </div>

</section>
