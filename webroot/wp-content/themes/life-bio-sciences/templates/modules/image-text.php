<?php
  $block_prefix   = ( isset( $section_var ) ) ? $section_var: 'image_text'; // allows us to reuse the component by "passing in" a variable
  $title          = get_field( $block_prefix . '_title' );
  $text           = get_field( $block_prefix . '_text' );
  $button_text    = get_field( $block_prefix . '_button_text' );
  $button_link    = get_field( $block_prefix . '_button_link' );
  $image          = get_field( $block_prefix . '_image' );
  $image_position = get_field( $block_prefix . '_image_position' );
?>
<section class="image-text-block layout-<?php echo $image_position; ?> ">
  <?php
    echo '<div class="container">';
    echo '  <div class="image">';
    if ( $image ) {
      echo wp_get_attachment_image( $image, 'original' );
    }
    echo '  </div>';
    echo '  <div class="text">';

    // text
    if ( $title ) {
      echo '<h2>' . $title . '</h2>';
    }
    if ( $text ) {
      echo $text;
    }

    // button
    if ( $button_text && $button_link ) {
      echo '<a href="' . $button_link . '" class="button-cta">' . $button_text . '</a>';
    }
    echo '  </div>';
    echo '</div>';
  ?>
</section>
