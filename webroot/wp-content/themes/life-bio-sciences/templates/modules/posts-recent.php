<section class="posts-recent">
  <div class="container">
    <h2 class="section-title"><?php _e( 'latest news', 'sage' ); ?></h2>
    <?php
      $args = array(
        'post_type'    => 'post',
        'post_status'  => 'publish',
        'posts_per_page' => '4'
      );
      $query = new WP_Query( $args );

      echo '<div class="posts">';
      if ( $query->have_posts() ) :
        while ( $query->have_posts() ) :
          $query->the_post();
          get_template_part('templates/content');
        endwhile;
        wp_reset_postdata();
      endif;
      echo '</div>';
    ?>
    <p><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="button-cta">
      <?php _e( 'View All', 'sage' ); ?>
    </a></p>
  </div>
</section>
