<?php
  /**
   * Load-bearing component. Consolidating with the Hero component is largely a waste of time.
   */

  $news_page_id = get_option( 'page_for_posts' );
  $title        = get_field('title', $news_page_id);
  $text         = get_field('text', $news_page_id);
  $image        = get_field('image', $news_page_id);
?>

<section class="hero theme-white layout-center">
  <div class="bg-container" style="background-image: url('<?php echo $image; ?>');">

    <?php include( locate_template( 'templates/modules/hero-bubbles.php' ) ); ?>

    <?php if(!is_front_page()): ?>
      <div class="breadcrumbs">
        Home > <?php single_post_title(); ?>
      </div>
    <?php endif; ?>

    <div class="container">
      <div class="content">
        <h1><?php echo $title ?></h1>
        <?php echo ($text) ? $text : '' ; ?>
      </div>
    </div>

  </div>

</section>
