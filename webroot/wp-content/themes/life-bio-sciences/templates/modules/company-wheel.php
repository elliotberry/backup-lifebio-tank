<?php /* Intro Body Copy */ ?>
<section class="company-list">
  <div class="container">
    <?php
      $intro_text = get_field('text');

      if ($intro_text) { ?>
        <div class="introduction">
          <?php echo get_field('text'); ?>
        </div>
      <?php }
    ?>
  </div>
</div>

<div class="cool-new-wheel-container">
  <?php
  $htmlstr = "";
  // focus areas
  $args = array(
    'post_type' => 'lifebio_company',
    'post_status' => 'publish',
    'order' => 'ASC',
    'orderby' => 'menu_order',
    'meta_key' => 'view_area',
    'meta_value' => 'wheel',
    'posts_per_page' => 1000
  );
  $query = new WP_Query($args);

  if ($query->have_posts()):
    $i = 0;
    while ($query->have_posts()):

      $query->the_post();
      $area_id = get_the_ID();
      $area_name = get_the_title();
      $location = get_field( 'location' );
      $area_color = get_field('color');
      $area_data = slugify($area_name);
      $logo = get_field('logo');
      $url = get_field( 'url' );

      if (get_field('summary')) {
        $desc = get_field('summary');
      } else {
        $desc = get_the_content();
      }

      if( have_rows('focus_areas') ):
        $focus_areas = get_field('focus_areas');

        // $fa__white_logos --    String: Markup for each possibly associated Focus Area
        //
        // “…and forgive us our sins, as we forgive those who trespass against us;”
        $fa__white_logos = implode(array_map(
          function($focus_a) {
            return '<div class="white-logo" style="background-image:url('
            . get_field_object('logo_white', $focus_a['focus_area']->ID)['value']
            . ');"></div>';
          },
          $focus_areas));
      endif;

      ?>

  <div class="cool-new-wheel-blob company" data="<?php echo $area_data; ?>" style="border-color:<?php echo $area_color; ?>;">
    <a class="area-logo" style=" background-image:url('<?php echo $logo; ?>');" data-href='<?php echo $url ?>' target='_blank' rel='noopener'>
      <div class="plus" style="color:<?php echo $area_color; ?>;">+</div>
    </a>
  </div>

  <?php
  $htmlstr =
    $htmlstr .
    '<div class="wheel-popup ' .
    $area_data .
    '" data="' .
    $area_data .
    '" style="background: ' .
    $area_color .
    '"><div class="blob-inside"><div class="area-name">' .
    ($url ? "<a href='$url' target='_blank' rel='noopener'>$area_name&nbsp;|&nbsp<span class='icon-external'></span></a>" : $area_name) .
    '<br/>' . ( $location ?: '') .
    '</div><div class="area-desc">' .
    $desc .
    '</div><a class="back"></a>' .
    ( $fa__white_logos ? "<div class='white-logo-wrappers'>$fa__white_logos</div>" : '' ) .
    '</div></div>';
  $i++;

    endwhile;
    wp_reset_postdata();
  endif; ?>

  <div class="middle-container">
    <img class="middle-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-life-biosciences.svg" alt="Life Biosciences">
    <div class="middle-bottom">
      <img src="/wp-content/themes/life-bio-sciences/assets/images/middle-sub-logos@2x.png" class="little-middle-logo">
    </div>
  </div>
  <div class="popup-container">
    <?php echo $htmlstr; ?>
  </div>
</div>
