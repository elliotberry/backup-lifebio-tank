<?php
  $text           = get_field( 'cta_text' );
  $button_text    = get_field( 'cta_button_text' );
  $button_link    = get_field( 'cta_button_link' );
?>
<section class="cta-block">
  <?php
    echo '<div class="container">';
    echo '  <div class="text">';
      if ( $text ) {
        echo '<h2>' . $text . '</h2>';
      }
    echo '  </div>';
    echo '  <div class="button-wrap">';
      if ( $button_text && $button_link ) {
        echo '<a href="' . $button_link . '" class="button-cta">' . $button_text . '</a>';
      }
    echo '  </div>';
    echo '</div>';
  ?>
</section>
