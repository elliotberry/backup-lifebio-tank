<?php
/**
 * Template Name: Focus Areas Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page-focusareas'); ?>
<?php endwhile; ?>
